require('colors');
const zmq = require("zeromq");
const Curve = require("./lib/Curve");
const serviceName = 'Data source service';

console.info(`${Date()} > ${serviceName} - ZeroMQ version: ${zmq.version}`.yellow);

const keysBuffer = zmq.curveKeyPair();
const curve = new Curve({name: serviceName});

let publicKey = curve.getKeyFromFile("public_key.curve");
if (!publicKey) {
    publicKey = keysBuffer.publicKey;
    curve.createKeyFile("public_key.curve", publicKey);
}

let secretKey = curve.getKeyFromFile("secret_key.curve");
if (!secretKey) {
    secretKey = keysBuffer.secretKey;
    curve.createKeyFile("secret_key.curve", secretKey);
}

console.info(`${Date()} > ${serviceName} - Keys: \n - public: ${publicKey} \n - secret: ${secretKey}`.magenta);
console.info(`${Date()} > ${serviceName} - Init complete.`.green);
