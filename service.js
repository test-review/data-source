#!/usr/bin/env node
require("colors");
const config = require("./config");
const zmq = require("zeromq");
const zlib = require("zlib");
const Curve = require("./lib/Curve");
const Entity = require("./lib/Entity");

const serviceName = "Data source service";

/**
 * @param config
 * @returns {string}
 */
function getConnectionString(config) {
    return `${config.transport}://${config.host}${config.port ? ':' + config.port : ''}`;
}

/**
 * @param pool {*[]}
 * @param min {Number}
 * @param max {Number}
 * @returns {Entity}
 */
function getRandomEntity(pool = [], min = 0, max = 0) {
    return pool[min - 1 + Math.floor((max - min + 1) * Math.random())];
}

try {
    let entityPool = [];
    // init entities
    for (let i = config.entities.from; i <= config.entities.size; i++) {
        let entityId = `Entity_${i}`;
        entityPool.push(new Entity(entityId, config.entities.params));
    }

    const curve = new Curve({name: serviceName});
    const pub = new zmq.Publisher;

    pub.curveServer = true;
    pub.curvePublicKey = curve.getKeyFromFile("public_key.curve");
    pub.curveSecretKey = curve.getKeyFromFile("secret_key.curve");
    pub.linger = 0;

    (async () => {
        await pub.bind(getConnectionString(config));
        console.info(`${Date()} > ${serviceName} - Publisher bound to port 5555`.cyan.bold);

        setInterval(function () {
            (async () => {
                const entity = getRandomEntity(entityPool, config.entities.from, config.entities.size);
                entity.generateParams();

                await zlib.deflate(JSON.stringify(entity.getJson()), (err, buffer) => {
                    if (!err) {
                        try {
                            pub.send([config.channel, buffer]);
                        } catch (e) {
                            throw new Error(`${Date()} > ${serviceName} - socket send error: ${e.message}`);
                        }
                    } else {
                        throw new Error(`${Date()} > ${serviceName} - data send error due compress: ${err}`);
                    }
                });

            })();
        }, config.messagePerTimeMs);

    })();

} catch (e) {
    console.error(`${Date()} > ${serviceName} - error: ${e.message}`);
}
