class Entity {
    /**
     * @type {{}}
     */
    params = {};

    /**
     * @param name
     * @param options
     */
    constructor(name, options) {
        this.name = name;

        this.paramsFrom = options.from;
        this.paramsSize = options.size;

        this.valueFrom = options.values["from"];
        this.valueTo = options.values["to"];
        this.digits = options.values["digits"];

        this.generateParams();
    }

    /**
     * Generate random value in range
     */
    generateValue() {
        return (this.valueFrom + (this.valueTo - this.valueFrom) * Math.random()).toFixed(this.digits);
    }

    /**
     * Generate entity params
     */
    generateParams() {
        this.params = {};
        for (let i = this.paramsFrom; i <= this.paramsSize; i++) {
            let paramId = `param_${i}`;
            this.params[paramId] = this.generateValue();
        }
    }

    /**
     * @returns {{} & {id: *}}
     */
    getJson() {
        return Object.assign(this.params, {"id": this.name});
    }
}

module.exports = Entity;
